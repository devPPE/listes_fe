import { createRouter, createWebHistory } from 'vue-router';
import CreateAccount from './CreateAccount.vue';

const routes = [
  // autres routes ici
  {
    path: '/signup',
    name: 'signup',
    component: CreateAccount,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;